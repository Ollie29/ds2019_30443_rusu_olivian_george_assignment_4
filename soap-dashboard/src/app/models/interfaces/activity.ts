export interface ActivityInterface {
    id: string;
    activityName: string;
    startDate: string;
    endDate: string;
}
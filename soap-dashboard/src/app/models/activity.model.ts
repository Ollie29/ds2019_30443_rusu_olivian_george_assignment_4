import { ActivityInterface } from './interfaces/activity';
import { Injectable } from '@angular/core';

@Injectable()
export class ActivityModel {
    all: Array<ActivityInterface>;
}
package com.example.springdemo.controller.errorhandler;

import com.example.springdemo.dto.ActivityDTO;
import com.example.springdemo.services.ActivityClientService;
import com.example.springdemo.wsdl.GetActivitiesForPatientResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "soap")
public class ActivityController {

    private final ActivityClientService activityClientService;

    @Autowired
    public ActivityController(ActivityClientService activityClientService) {
        this.activityClientService = activityClientService;
    }

    @GetMapping(value = "/get/activities/{patientID}")
    public List<ActivityDTO> getActivities(@PathVariable("patientID") int patientID) {
        return activityClientService.getActivities(patientID);
    }
}

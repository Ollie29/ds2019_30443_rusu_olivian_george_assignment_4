package com.example.springdemo.repositories;

import com.example.springdemo.entities.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public interface ActivityRepository extends JpaRepository<Activity, Integer> {

   List<Activity> findActivitiesByPatientId(int patientID);
}
